const express = require('express')
const router = express.Router()
const gameController = require('./controllers/gameController');
const { body } = require('express-validator')
const checkToken = require('./middleware/checkToken')

const biodataController = require('./controllers/biodataController');
const historyController = require('./controllers/historyController');



router.get('/game/list', gameController.list)
router.post('/game/register', gameController.create)
router.put('/game/update', gameController.update)
router.delete('/game/destroy', gameController.destroy)

router.post('/game/login', body('email').isEmail().notEmpty(), body('password').notEmpty(), gameController.login)
router.get('game/profile', checkToken, gameController.getProfile)

router.get('/biodata/list', biodataController.list)
router.post('/biodata/create', biodataController.create)
router.put('/biodata/update', biodataController.update)
router.delete('/biodata/destroy', biodataController.destroy)

router.get('/history/list', historyController.list)
router.post('/history/create', historyController.create)
router.put('/history/update', historyController.update)
router.delete('/history/destroy', historyController.destroy)



module.exports = router

const { User_Game } = require('../models');
const bcrypt = require('bcrypt')
const { body, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')


const user_game_biodata = require('../models/user_game_biodata');
const user_game_history = require('../models/user_game_history');

module.exports = {
    list: async (req,res) => {
        try {
            const data = await User_Game.findAll({
                // include: [
                //     {model:user_game_biodata},
                //     {model:user_game_history}
                // ]
            });
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req,res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array()
                });
            }
            const password = bcrypt.hashSync(req.body.password, 10)
            const data = await User_Game.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: password
                
            });

            return res.json({
                data:data
            })

        } catch (error) {
            console.log(error)
            return res.json({
                message: "Fatal Error!"
            })
        }
    },
    update: async (req,res) => {
        try {
            const data = await User_Game.update({
                name: req.body.name
            }, {
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req,res) => {
        try {
            const data = await User_Game.destroy({
                where : {
                    id: req.body.id
                }
            }
            );
            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    login: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const user = await User_Game.findOne({ where : {
                email: req.body.email
            }})

            if(!user) {
                return res.json({
                    message : "Admin not exists!"
                })
            }

            if (bcrypt.compareSync(req.body.password, user.password)) {
                const token = jwt.sign(
                  { id: user.id },
                  'secret',
                  { expiresIn: '6h' }
                )
          
                return res.status(200).json({
                    token: token
                })
              }
            
            return res.json({
                message : "Wrong Password!"
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    getProfile: async(req, res) => {
        try {
            const user = await User_Game.findOne({ where : {
                id: res.user.id
            }})
            
            return res.json({
                data : user
            })
            
        } catch (error) {
            console.log(error)
            return res.json({
                message : "Fatal Error!"
            })
        }
    }

}
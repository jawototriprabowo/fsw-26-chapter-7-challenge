const express = require('express');
const app = express();
require('dotenv').config();
const cors = require('cors');
const port = process.env.PORT;
const router = require('./router')



app.set('view engine','ejs')
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true}));
app.use(router)

app.use(express.static(__dirname + '/public'));


app.get('/',(req,res) =>{
    res.render('home');
})
app.get('/game',(req,res) =>{
    res.render('game');
})

app.listen(port, () => {
    console.log(`Exemple app listening on port ${port}`)
});



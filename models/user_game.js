'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.user_game_biodata, {
        foreignKey: 'user_biodata_id'
      }),
      this.hasMany(models.user_game_history, {
        foreignKey: 'user_historis_id'
      })
    }
  }
  User_Game.init({
    first_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }    
  }, {
    sequelize,
    modelName: 'User_Game',
  });
  return User_Game;
};